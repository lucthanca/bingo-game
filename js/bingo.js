var players;
var numSquare1 = 5;
var numSquare2 = 5;
var clear_Interval;
var countDownSecond = 1;
var usedNums;
var Cards = [];
var preNum = "";
// Loop background sound
$(document).ready(function () {
    var bg_sound = new Audio('sound.mp3');
    bg_sound.loop = true;
    bg_sound.play();
});

//menu task
$(document).ready(function () {
    $('._options').on('click', function (param) {
        $(this).parent().css({ 'display': 'none' });
        $(this).parent().next().css({ 'display': 'block' });
    });
    $('._ok_options').on('click', function (e) {
        $('._option').css({ 'display': 'none' });
        $('.__main-menu').css({ 'display': 'block' });
    });
    $('._new-game').on('click', function (params) {
        players = $('#players').val();
        times = $('#times').val();
        // todo : validation và run
        console.log(players + "_" + numSquare1 + "_" + numSquare2)
        if (players != "" && parseInt(players) > 0 && times != "" && parseInt(times) > 0) {
            if (parseInt(players) <= 100 && parseInt(times) <= 60) {
                $('._main-menu').css({ 'display': 'none' });
                GenerateCards(players, numSquare1, numSquare2);
                $('._main-content').css({ 'display': 'block' });
            } else {
                if (parseInt(players) > 100) {
                    swal({
                        title: "Nhắc nhẹ!",
                        text: "Tận hơn 100 người chơi cơ á?!",
                        icon: "warning",
                        buttons: {
                            cancel: true,
                            confirm: "Ừa !",
                            cancel: "Không ! Đi đến cài đặt."
                        },
                        dangerMode: true,
                    }).then((isDeleted) => {
                        if (isDeleted) {
                            if (parseInt(players) > 1000) {
                                swal("Nhắc nhẹ !", "Số lượng người chơi hình như hơi lớn quá mức cho phép thì phải.\n (< 1000 người thôi) !", "error");
                                $('._option').css({ 'display': 'block' });
                                $('.__main-menu').css({ 'display': 'none' });
                            }
                            else {
                                $('._main-menu').css({ 'display': 'none' });
                                GenerateCards(players, numSquare1, numSquare2);
                                $('._main-content').css({ 'display': 'block' });
                            }
                        } else {
                            $('._option').css({ 'display': 'block' });
                            $('.__main-menu').css({ 'display': 'none' });
                            $('#players').focus();
                        }
                    });
                }

                if (parseInt(times) > 60) {
                    swal({
                        title: "Nhắc nhẹ!",
                        text: "Thời gian giữa các lần phát số tận hơn 1 phút cơ à?!",
                        icon: "warning",
                        buttons: {
                            cancel: true,
                            confirm: "Ừa !",
                            cancel: "Không ! Đi đến cài đặt."
                        },
                        dangerMode: true,
                    }).then((isDeleted) => {
                        if (isDeleted) {
                            if (parseInt(times) > 1000) {
                                swal("Nhắc nhẹ !", "Thời gian hơi lâu à nha !.\n (tầm < 60s thôi, cho người chơi đỡ xót ruột !) !", "error");
                                $('._option').css({ 'display': 'block' });
                                $('.__main-menu').css({ 'display': 'none' });
                            }
                            else {
                                $('._main-menu').css({ 'display': 'none' });
                                GenerateCards(players, numSquare1, numSquare2);
                                $('._main-content').css({ 'display': 'block' });
                            }
                        } else {
                            $('._option').css({ 'display': 'block' });
                            $('.__main-menu').css({ 'display': 'none' });
                            $('#times').focus();
                        }
                    });
                }
            }
            //console.log(players);
        } else {

            if (players === "" || parseInt(players) <= 0) {
                $('._option').css({ 'display': 'block' });
                $('.__main-menu').css({ 'display': 'none' });
                $('#players').css({
                    'outline-color': 'rgba(255, 0, 0, 0.75)',
                    'outline-style': 'auto',
                    'background': 'rgba(255, 0, 0, 0.05)',
                });
            }
            if (times === "" || parseInt(times) <= 0) {
                console.log('rỗng');
                $('._option').css({ 'display': 'block' });
                $('.__main-menu').css({ 'display': 'none' });
                $('#times').css({
                    'outline-color': 'rgba(255, 0, 0, 0.75)',
                    'outline-style': 'auto',
                    'background': 'rgba(255, 0, 0, 0.05)',
                });
            }
        }
    });

    jQuery(document).bind("keyup keydown", function (e) {
        // if(e.ctrlKey && e.keyCode == 80){
        //     $('._control-task').css({'display':'none'});
        //     //alert('ctrl + P');
        // }
        // else 
        // if (e.keyCode === 27){
        //     $('._control-task').css({'display':'block'});
        // }
    });
});


$(document).ready(function () {
    // reset focus
    $('#players').focus();
    $(document).on('focus', '#players', function () {
        $(this).css({
            'outline': 'none',
            'background': 'transparent',
        });
    });
});



// todo : Lấy một số ngẫu nhiên trong khoảng min -> max
function GetRan(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
    // floor : trả về 1 số nguyên lớn nhất nhỏ hơn hoặc bằng số thực đang xét
    // random : trả về 1 số giữa 2 số 0 và 1
}
// end func
// todo : Sinh 1 mảng ngẫu nhiên với số lượng phần tử truyền vào
function GetArray(capacity) {
    var _returnArray = [];
    // Biến cờ
    var flag = 1;
    while (flag <= capacity) {
        // todo : lấy 1 số ngẫu nhiên
        var ranNum = GetRan(1, 100);
        // Todo : Kiểm tra xem số đã có trong mảng hay chưa
        //        Nếu chưa thì add, tăng biến cờ hoặc ngược lại
        if (_returnArray.indexOf(ranNum) == -1) {
            _returnArray.push(ranNum);
            flag++;
        }
    }
    return _returnArray;
}
// end

// todo: Sinh mã html
function GenerateHtml(card, numPage, numSquare1, numSquare2) {
    var mainCardHtml_footer = `</tr></table><div class="footer"><h6>75 ball bingo card</h6></div></div></div>`;
    let mainCardHtml = `<page size="A4">`;
    // *** Chẵn ***
    //  1x4+0 = 4 -> 1; => 4-4+1  = 1       cardx4+card_le = 4 -> 1; => cardx4+1-4 = 1
    //  2x4+0 = 8 -> 5; => 8-4+1 = 5
    //  3x4+0 = 12 -> 9; => 12-4+1 = 9
    //  4x4+0 = 16 -> 13; => 16-4+1 = 13
    // *** lẻ ***
    // 
    //  4x4+3 = 19 -> 17 => 19-3+1 = 17     cardx4+card_le = 19 -> 17 => cardx4+1-3 = 17
    var capacity = numSquare1 * numSquare2;
    console.log(Math.floor((capacity) / 2));
    for (var item = 0; item < numPage; item++) {
        var bingoArray = GetArray(capacity);
        Cards.push(bingoArray);
        mainCardHtml += `<div class="box box-info bcard">
            <div class="box-header bcard-header">
                <img src="images/Bingo-footer.png" alt="bingo">
            </div>
            <div class="box-body bcard-body">
              <table class="table table-light bingo-card" card = "${card}">`;
        for (var i = 0; i < bingoArray.length; i++) {

            if (i == 0) {
                mainCardHtml += `<tr><td>${bingoArray[i]}</td>`;
            }
            else if (i % numSquare1 == 0) {
                mainCardHtml += `</tr><tr><td>${bingoArray[i]}</td>`;
                //console.log(i+ ' true');
            } else if (i == Math.floor((capacity) / 2)) {
                mainCardHtml += `<td class="free">FREE</td>`;
                //console.log(i);
            }
            else {
                mainCardHtml += `<td>${bingoArray[i]}</td>`;
            }
        }
        mainCardHtml += mainCardHtml_footer;
    }
    mainCardHtml += `</page>`;
    return mainCardHtml;
}
// end

// In ra phiếu sau khi đã validate dữ liệu
function GenerateCards(players, numSquare1, numSquare2) {
    $('._main-content').html("");
    $('._main-content').html(`<div class="_control-btn">
    <div class="control">
    <!-- Return -->
      <div class="block-button button-style7">
          <a href="#" class="button" id="_return" title="Back to menu game">
            <span class="ico ion-arrow-return-left" ></span>
          </a>
        </div>
        <!-- Refresh -->
        <div class="block-button button-style1">
          <a href="#" class="button" id="_generate" title="Re-create Bingo Cards">
            <span class="ico ion-ios-refresh" ></span>
          </a>
        </div>
        <div class="block-button button-style6">
            <a href="#" class="button" id="_start" title="Start Game">
              <span class="ico ion-ios-play" ></span>
            </a>
          </div>
    </div>
        <div ><h1 id="foo">${countDownSecond - 1}</h1></div>
  </div>`);
    if (players % 4 == 0) {
        for (var i = 1; i <= players / 4; i++) {
            $('._main-content').append(GenerateHtml(i, 4, numSquare1, numSquare2));
        }
    } else {
        for (var i = 1; i <= Math.floor(players / 4); i++) {
            $('._main-content').append(GenerateHtml(i, 4, numSquare1, numSquare2));
        }
        $('._main-content').append(GenerateHtml(Math.floor(players / 4), players % 4, numSquare1, numSquare2));
    }
    console.log(Math.floor(players / 4) + 1);

}

// Control btn task
$(document).ready(function () {
    // todo : tạo mảng lấy số để quay
    usedNums = GetArray(100);
    $(document).on('click', '#_generate', function () {
        if ($('#_start').hasClass('_tiepTuc')) {
            clearInterval(clear_Interval);
            swal({
                title: "Nhắc nhẹ!",
                text: "Bạn có muốn tạo lại Bingo card trong khi game đang chơi?!",
                icon: "warning",
                buttons: {
                    cancel: true,
                    confirm: "Ừm",
                    cancel: "Mình ấn nhầm"
                },
                dangerMode: true,
            }).then((isDeleted) => {
                if (isDeleted) {

                    $('#_start').removeClass('_tiepTuc');
                    countDownSecond = 1;
                    $('#_start').attr('title', 'Start Game');
                    $('#_start').children().removeClass('ion-pause').addClass('ion-ios-play');
                    GenerateCards(players, numSquare1, numSquare2);
                    swal("Đã tạo mới Bingo Cards!", {
                        icon: "success",
                    });
                } else {
                    CountDown();
                }
            });
        } else {
            GenerateCards(players, numSquare1, numSquare2);
        }
        //console.log(GetArray(100));
    });
    $(document).on('click', '#_return', function () {

        var _start_btn = $('#_start');
        if (_start_btn.hasClass('_tiepTuc')) {
            clearInterval(clear_Interval);
            swal({
                title: "Nhắc nhẹ!",
                text: "Bạn có muốn quay lại menu game trong khi game đang bắt đầu?!",
                icon: "warning",
                buttons: {
                    cancel: true,
                    confirm: "Ừm",
                    cancel: "Mình ấn nhầm"
                },
                dangerMode: true,
            }).then((isDeleted) => {
                if (isDeleted) {

                    $('#_start').removeClass('_tiepTuc');
                    countDownSecond = 1;
                    $('#_start').attr('title', 'Start Game');
                    $('#_start').children().removeClass('ion-pause').addClass('ion-ios-play');
                    $('._main-content').css({ 'display': 'none' });
                    $('._main-menu').css({ 'display': 'block' });
                } else {
                    CountDown();
                }
            });

        }
        else {
            $('#_start').removeClass('_tiepTuc');
            countDownSecond = 1;
            $('#_start').attr('title', 'Start Game');
            $('#_start').children().removeClass('ion-pause').addClass('ion-ios-play');
            $('._main-content').css({ 'display': 'none' });
            $('._main-menu').css({ 'display': 'block' });
        }
    });
    $(document).on('click', '#_start', function () {
        if (!$('#_start').hasClass('_tiepTuc')) {
            //console.log('có');
            //$(this).text('Tiếp tục');
            $('page').remove();
            let htmlStr = $('._main-content').html();
            if (htmlStr.indexOf('_list-used-num') == -1) {
                htmlStr = `<div class="_control-btn">
<div class="control">
  <!-- Return -->
  <div class="block-button button-style7">
    <a href="#" class="button" id="_return" title="Back to menu game">
      <span class="ico ion-arrow-return-left"></span>
    </a>
  </div>
  <!-- Refresh -->
  <div class="block-button button-style1">
    <a href="#" class="button" id="_generate" title="Re-create Bingo Cards">
      <span class="ico ion-ios-refresh"></span>
    </a>
  </div>
  <div class="block-button button-style6">
    <a href="#" class="button" id="_start" title="Start Game">
      <span class="ico ion-ios-play"></span>
    </a>
  </div>
</div>
<div>
  <h1 id="foo"></h1>
</div>
</div>
<div class="_list-used-num">
<div class="content">
    <div class="_recent-num">
        <div><h1>Số mới quay gần nhất</h1></div>
        <ul>
        </ul>
      </div>
      <div class="_used-num">
        <div><h1>Các số đã quay</h1></div>
        <ul>
          
        </ul>
      </div>
</div>
</div>`;

            }
            $('._main-content').html(htmlStr);
            $('#_start').addClass('_tiepTuc');
            $('#_start').attr('title', 'Tạm dừng');
            $('#_start').children().removeClass('ion-ios-play').addClass('ion-pause');

            CountDown();
        }
        else {
            clearInterval(clear_Interval);
            $('#_start').removeClass('_tiepTuc');
            $('#_start').attr('title', 'Tiếp tục');
            $('#_start').children().removeClass('ion-pause').addClass('ion-ios-play');
        }


    });

    // Count down

    function CountDown() {

        var times = $('#times').val();
        console.log(times);
        // Todo lấy ra random index của usedNums và đọc số đó lên rồi 
        // xoá số đó khỏi usedNums
        //countDownSecond = 1;
        
        var voices = $('#voices').children('option:selected').val();
        clear_Interval = setInterval(function () {
            if (countDownSecond == parseInt(times) + 1) {
                countDownSecond = 1;
                var ranindex = GetRan(0, usedNums.length);
                var chuDoced = usedNums[ranindex].toString();
                responsiveVoice.speak(chuDoced, voices);
                deletedNum = usedNums.splice(ranindex, 1);
                $('._control-btn div h1').html(chuDoced);
                if (preNum != "") {
                    $('._recent-num').children().next().html(`<li class="circle"><h1>${preNum}</h1></li>`);
                }
                preNum = chuDoced;
                $('._used-num').children().next().append(`<li><h1>${chuDoced}</h1></li>`);
                console.log(usedNums);

            } else {
                console.log(countDownSecond);
                //$('._control-btn div h1').html(countDownSecond);
                countDownSecond++;
                var audio = new Audio('tick.m4a');
                audio.play();
            }

        }, 1000);
    }
});